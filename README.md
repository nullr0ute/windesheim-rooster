# Windesheim Rooster

NodeJS server for fetching latest school schedules from the Windesheim API with NuxtJS front-end



# Installation

Clone the repo and copy .env.example to .env
```
mv .env.example .env
```

Change the .env file according to your prefference:
- using the up and running production back-end; api.windesheimrooster.nl
- using your own mostly up and running back-end; localhost:3005

## Start back-end (API)
To start the Windesheim Rooster server run the following command:

```
node server/
```

Or start a Docker container:

```
docker build .
```
## Start Nuxt.js front-end

```
yarn dev
```
