require("dotenv").config();
import vueNL from "vuetify/es5/locale/nl";

export default {
  mode: "spa",
  generate: {
    fallback: true
  },
  /*
   ** Headers of the page
   */

  head: {
    titleTemplate: "%s",
    title: "Windesheim Rooster | Bekijk je klasrooster | Check je volgende les",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { name: 'keywords', content: 'windesheim, rooster, klas, lessen, inzien, docent, leerling, student, google agenda, webcal, ical, cal, download, agenda, link, ict rooster, klassen rooster, check les, in agenda, 2019 - 2020, app'},
      {
        hid: "description",
        name: "description",
        content: "Je Windesheim rooster gemakkelijk en supersnel ophalen. Voeg met deze app je rooster direct toe aan je agenda en abonneer op je rooster zodat je altijd up to date bent."
      },
      {
        property: "og:type",
        content: "website"
      },
      {
        property: "og:url",
        content: "https://windesheimrooster.nl/"
      },
      {
        property: "og:title",
        content: "Windesheim Rooster | Bekijk je klasrooster | Check je volgende les"
      },
      { 
        property: "og:description",
        content: "Je Windesheim rooster gemakkelijk en supersnel ophalen. Voeg met deze app je rooster direct toe aan je agenda en abonneer op je rooster zodat je altijd up to date bent."
      },
      {
        property: "og:image",
        content: "https://windesheimrooster.nl/icon.png"
      },
      {
        property: "twitter:card",
        content: "summary_large_image"
      },
      {
        property: "twitter:url",
        content: "https://windesheimrooster.nl/"
      },
      {
        property: "twitter:title",
        content: "Windesheim Rooster | Bekijk je klasrooster | Check je volgende les"
      },
      {
        property: "twitter:description",
        content: "Je Windesheim rooster gemakkelijk en supersnel ophalen. Voeg je rooster direct toe aan je agenda en abonneer op je rooster zodat je altijd up to date bent."
      },
      {
        property: "twitter:image",
        content: "https://windesheimrooster.nl/icon.png"
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  env: {
    apiUrl: process.env.API_URL
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#FFC92A" },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/vuetify"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    "nuxt-clipboard2",
    "cookie-universal-nuxt",
    [
      "@nuxtjs/axios",
      {
        baseURL: process.env.API_URL
      }
    ],
    "@nuxtjs/google-analytics",
    [
      "@nuxtjs/google-adsense",
      {
        id: "ca-pub-3955605370008148"
      }
    ]
  ],
  googleAnalytics: {
    id: "UA-76194988-10",
    trackEvent: true
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    lang: {
      locale: [vueNL],
      current: "vueNL"
    },
    theme: {
      themes: {
        light: {
          primary: "#fbba00",
          secondary: "#289B38",
          success: "#45b97c",
          accent: "#604580",
          error: "#b71c1c"
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
};
