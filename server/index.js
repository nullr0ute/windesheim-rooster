const fs = require("fs");
const env = require("dotenv").config();
const moment = require("moment");
const tz = require("moment-timezone");
const request = require("request");
const ical = require("ical-generator");

const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const port = process.env.SERVER_PORT;
const winApiCookie = process.env.WIN_API_COOKIE;
let winApiCookieValid = true;

const winApiUrl = "http://api.windesheim.nl/api/";
const winAuthApiUrl = "https://windesheimapi.azurewebsites.net/api/v1/";

let classes = [];
let subscribers = [];

let calStorage = [];

moment.tz.setDefault("Europe/Amsterdam");

let makeCal = klas => {
  calStorage[klas] = ical({
    domain: "windesheimrooster.nl",
    name: "Rooster van klas"
  });

  calStorage[klas].prodId({
    company: "Windesheim",
    product: "Rooster " + klas,
    language: "NL"
  });
};

if (fs.existsSync("server/subscribers.json")) {
  let subscribers_raw_text = fs.readFileSync("server/subscribers.json");
  if (subscribers_raw_text.length > 0) {
    subscribers = JSON.parse(subscribers_raw_text);
  } else {
    subscribers = [];
    fs.writeFileSync("server/subscribers.json", "[]");
  }
} else {
  subscribers = [];
  fs.writeFileSync("server/subscribers.json", "[]");
}

subscribers.forEach(subscriber => {
  console.log("➡️ Subscribing klas " + subscriber);
  makeCal(subscriber);
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  var allowedOrigins = [
    "http://localhost:3000",
    "http://windesheimrooster.test:3000",
    "https://windesheimrooster.nl"
  ];
  var origin = req.headers.origin;
  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader("Access-Control-Allow-Origin", origin);
  }
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

let searchKlas = klas => {
  return classes.filter(classs => classs.klasnaam.toUpperCase().includes(klas));
};
let searchVak = vak => {
  return vakken.filter(vakk => vakk.code.toUpperCase().includes(vak));
};

app.get("/search/klas/:klas", (request, response) => {
  response.json(searchKlas(request.params.klas.toUpperCase()));
});
app.get("/search/vak/:vak", (request, response) => {
  response.json(searchVak(request.params.vak.toUpperCase()));
});

app.get("/klas/:klas", (request, response) => {
  let klasSearchResult = searchKlas(request.params.klas.toUpperCase());
  if (klasSearchResult.length > 0) {
    let klas_naam = klasSearchResult[0].klasnaam;
    FetchClassLessen(klas_naam, lessen => {
      addSubscriber(klas_naam);
      response.status(200).json(lessen);
    });
  } else {
    response.status(404).json({
      status: 404,
      msg: "Klas " + request.params.klas + " not found."
    });
  }
});

let addSubscriber = klas => {
  if (!subscribers.includes(klas)) {
    console.log("➕➕ Added new class-subscriber " + klas);
    makeCal(klas);
    subscribers.push(klas);
    fs.writeFileSync("server/subscribers.json", JSON.stringify(subscribers));
  }
};

app.post("/journey", function(req, res) {
  // Get lat and lon from request body and use them to determine closes station (hardcoded for now)
  var latitude = req.body.latitude;
  var longitude = req.body.longitude;
  // Hardcoded the depart station for now, waiting on NS API keys
  let departStation = "Nijverdal";
  // Convert epoch to human time, subtract two hours (7200000ms) due to timezone magic
  let arriveby = moment
    .unix((req.body.arriveby - 7200000) / 1000)
    .format("YYYY-MM-DDTHH:mm");
  // Build the request url
  let requestUrl = `https://www.ns.nl/rio-reisinfo-api/service/reisadvies?aankomstlocatie=TREINSTATION-Zwolle&lang=nl&onbeperktReizen=false&overstaptijd=0&tijdstip=${arriveby}&toonHSL=true&type=AANKOMST&vertreklocatie=TREINSTATION-${departStation}`;

  // Send the response containing some of the POST data for debugging and the next three journeys
  request(requestUrl, (err, _res, body) => {
    if (err) {
      return console.log(err);
    }

    res.status(200).send({
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      requesturl: requestUrl,
      arriveby: arriveby,
      journeys: JSON.parse(body)["reismogelijkheden"].splice(0, 3)
    });
  });
});

app.get("/vak/:vak/ical", (request, response) => {
  let vakSearchResult = searchVak(request.params.vak.toUpperCase());
  if (vakSearchResult.length > 0) {
    FetchVakLessen(vakSearchResult[0].code, lessen => {
      let vak_naam = vakSearchResult[0].code;
      addSubscriber(vak_naam);
      console.log("⏳ CalStorage for lessen from klas " + vak_naam);
      if (calStorage[vak_naam]) {
        calStorage[vak_naam].clear();

        lessen.forEach(les => {
          let starttijd = moment(les.starttijd)
              .tz("GMT")
              .format("YYYYMMDDTHHmmss"),
            eindtijd = moment(les.eindtijd)
              .tz("GMT")
              .format("YYYYMMDDTHHmmss"),
            docentenString = "";
          if (les.docentnamen) {
            for (let i = 0; i < les.docentnamen.length; i++) {
              if (i > 0) docentenString += ", ";
              docentenString += les.docentnamen[i];
            }
          }
          calStorage[vak_naam].createEvent({
            start: starttijd,
            end: eindtijd,
            timezone: "Europe/Amsterdam",
            summary: les.commentaar,
            description: docentenString + "\n" + les.commentaar,
            location: les.lokaal + " - Campus 2, 8017 CA Zwolle",
            url: "https://windesheimrooster.nl/vak/" + vak_naam
          });
        });
        calStorage[vak_naam].serve(response, "Rooster " + vak_naam + ".ics");
      } else {
        response.status(404).json({
          status: 404,
          msg: "Calendar for " + request.params.klas + " not found."
        });
      }
    });
  } else {
    response.status(404).json({
      status: 404,
      msg: "Vak " + request.params.klas + " not found."
    });
  }
});
app.get("/klas/:klas/ical", (request, response) => {
  let klasSearchResult = searchKlas(request.params.klas.toUpperCase());
  if (klasSearchResult.length > 0) {
    FetchClassLessen(klasSearchResult[0].klasnaam, lessen => {
      let klas_naam = klasSearchResult[0].klasnaam;
      addSubscriber(klas_naam);
      console.log("⏳ CalStorage for lessen from klas " + klas_naam);
      if (calStorage[klas_naam]) {
        calStorage[klas_naam].clear();

        lessen.forEach(les => {
          let starttijd = moment(les.starttijd)
              .tz("GMT")
              .format("YYYYMMDDTHHmmss"),
            eindtijd = moment(les.eindtijd)
              .tz("GMT")
              .format("YYYYMMDDTHHmmss"),
            docentenString = "";
          if (les.docentnamen) {
            for (let i = 0; i < les.docentnamen.length; i++) {
              if (i > 0) docentenString += ", ";
              docentenString += les.docentnamen[i];
            }
          }
          calStorage[klas_naam].createEvent({
            start: starttijd,
            end: eindtijd,
            timezone: "Europe/Amsterdam",
            summary: les.commentaar,
            description: docentenString + "\n" + les.commentaar,
            location: les.lokaal + " - Campus 2, 8017 CA Zwolle",
            url: "https://windesheimrooster.nl/klas/" + klas_naam
          });
        });
        calStorage[klas_naam].serve(response, "Rooster " + klas_naam + ".ics");
      } else {
        response.status(404).json({
          status: 404,
          msg: "Calendar for " + request.params.klas + " not found."
        });
      }
    });
  } else {
    response.status(404).json({
      status: 404,
      msg: "Klas " + request.params.klas + " not found."
    });
  }
});

app.listen(port, () => {
  console.log("🚀 Server running at http://127.0.0.1:" + port);
});

const isJSON = str => {
  if (/^\s*$/.test(str)) return false;
  str = str.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@");
  str = str.replace(
    /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
    "]"
  );
  str = str.replace(/(?:^|:|,)(?:\s*\[)+/g, "");
  return /^[\],:{}\s]*$/.test(str);
};

let WindesheimRequest = (endpoint, callback) => {
  // console.log("CookieValid " + winApiCookieValid);
  let apiUrl = winApiCookieValid ? winAuthApiUrl : winApiUrl;
  let winApiCookieValidClone = winApiCookieValid;
  request(
    apiUrl + endpoint,
    {
      headers: {
        Cookie: winApiCookieValid ? ".AspNet.Cookies=" + winApiCookie : ""
      }
    },
    (err, res, body) => {
      if (err) {
        return console.log(err);
      }
      if (!isJSON(body)) {
        console.log(
          "🚨 Windesheim API Request " +
            apiUrl +
            endpoint +
            " responded with no valid JSON."
        );
        if (winApiCookieValidClone) {
          winApiCookieValid = false;
          WindesheimRequest(endpoint, callback);
        }
        return;
      }
      callback(res, body);
    }
  );
};

let FetchAllClasses = (forceRefresh = false) => {
  console.log("FORCE REFRESH ::: " + forceRefresh);
  if (!forceRefresh && fs.existsSync("server/class_cache/Classes.json")) {
    console.log("⏳ Loading Classes Cache...");
    fs.readFile("server/class_cache/Classes.json", (err, data) => {
      try {
        classes = JSON.parse(data);
        if (classes.length == 0) {
          console.log(
            "🚨 Problem occurd while loading class cache, 0 classes found..."
          );
          FetchAllClasses(true);
        } else {
          console.log(
            "✅ Loaded " + classes.length + " classes from the Classes cache"
          );
        }
      } catch (error) {
        console.log("🚨 Problem occurd while loading class cache: " + error);
        FetchAllClasses(true);
      }
    });
  } else {
    console.log("⏳ No classes cache found, fetching now...");
    WindesheimRequest("Klas/", (res, body) => {
      console.log("✅ Succesfully fetched Class Cache");
      if (!fs.existsSync("server/class_cache"))
        fs.mkdirSync("server/class_cache");
      fs.writeFileSync("server/class_cache/Classes.json", body, () => {
        console.log("✅ Written all classes to cache");
      });
      classes = JSON.parse(body);
    });
  }

  if (!forceRefresh && fs.existsSync("server/class_cache/Vakken.json")) {
    console.log("⏳ Loading Vakken Cache...");
    fs.readFile("server/class_cache/Vakken.json", (err, data) => {
      try {
        vakken = JSON.parse(data);
        if (vakken.length > 0) {
          console.log(
            "✅ Loaded " + classes.length + " vakken from the Vakken cache"
          );
        }else{
          console.log(
            "🚨 Problem occurd while loading vakken cache, 0 vakken found..."
          );
          FetchAllClasses(true);
        }
      } catch (error) {
        console.log("🚨 Problem occurd while loading vakken cache: " + error);
        FetchAllClasses(true);
      }
    });
  } else {
    console.log("⏳ No vakken cache found, fetching now...");
    WindesheimRequest("Vak/", (res, body) => {
      console.log("✅ Succesfully fetched Class Cache");
      if (!fs.existsSync("server/class_cache"))
        fs.mkdirSync("server/class_cache");
      fs.writeFileSync("server/class_cache/Vakken.json", body, () => {
        console.log("✅ Written all vakken to cache");
      });
      vakken = JSON.parse(body);
    });
  }
};

let FetchVakLessen = (vak, callback = null, forceFetch = false) => {
  const cache_path = "server/class_cache/" + vak + ".json";
  console.log("⏳ Fetching Lessen for vak " + vak + "...");
  // if (!forceFetch && fs.existsSync(cache_path)) {
  //   console.log("✅ Klas " + klas + " exists in cache, reading out...");

  //   fs.readFile(cache_path, (err, lessen) => {
  //     try {
  //       if (callback) callback(JSON.parse(lessen));
  //     } catch (error) {
  //       console.log(error);
  //     }
  //   });
  // } else {
  console.log(
    "⏳ Vak " +
      vak +
      " doesn't exist in cache or is forced to 🔄, requesting..."
  );
  WindesheimRequest("Vak/" + vak + "/Les?$orderby=starttijd", (res, body) => {
    let parsedBody = JSON.parse(body);
    console.log(
      "✅ " +
        res.statusCode +
        " :: " +
        new Date().toTimeString() +
        " body_length:" +
        parsedBody.length
    );
    // appendKlasLesData(klas, parsedBody);
    if (callback) callback(JSON.parse(body));
  });
  // }
};

let FetchClassLessen = (klas, callback = null, forceFetch = false) => {
  const cache_path = "server/class_cache/" + klas + ".json";
  console.log("⏳ Fetching Lessen for " + klas + "...");
  if (!forceFetch && fs.existsSync(cache_path)) {
    console.log("✅ Klas " + klas + " exists in cache, reading out...");

    fs.readFile(cache_path, (err, lessen) => {
      try {
        if (callback) callback(JSON.parse(lessen));
      } catch (error) {
        console.log(error);
      }
    });
  } else {
    console.log(
      "⏳ Klas " +
        klas +
        " doesn't exist in cache or is forced to 🔄, requesting..."
    );
    WindesheimRequest(
      "Klas/" + klas + "/Les?$orderby=starttijd",
      (res, body) => {
        let parsedBody = JSON.parse(body);
        console.log(
          "✅ " +
            res.statusCode +
            " :: " +
            new Date().toTimeString() +
            " body_length:" +
            parsedBody.length
        );
        appendKlasLesData(klas, parsedBody);
        if (callback) callback(JSON.parse(body));
      }
    );
  }
};

let getLesId = les => {
  return les.commentaar + "~" + les.starttijd + "~" + les.eindtijd;
};

let appendKlasLesData = (klas, parsedLesdata) => {
  const cache_path = "server/class_cache/" + klas + ".json";
  if (fs.existsSync(cache_path)) {
    FetchClassLessen(klas, existingLessen => {
      // filter file content on lessons from the past
      let pastCacheLessons = existingLessen.filter(les => {
        return les.eindtijd <= moment();
      });
      // get array for all past lessons
      let pastLessonIdentifiers = pastCacheLessons.map(les => getLesId(les));
      // filter past lessons to remove duplicates
      let pastLessons = pastCacheLessons.filter(les => {
        return !pastCacheLessons.includes(getLesId(les));
      });
      // save lessons from the future (simple api) that do not exist yet
      let newLessons = parsedLesdata.filter(les => {
        return !pastLessonIdentifiers.includes(getLesId(les));
      });

      try {
        // concat past and fresh and write file
        let lessen = pastLessons.concat(newLessons);
        fs.writeFile(cache_path, JSON.stringify(lessen), () => {});
      } catch (error) {}
    });
  } else {
    // just write - no existing append
    try {
      fs.writeFile(cache_path, JSON.stringify(parsedLesdata), () => {});
    } catch (error) {}
  }
};

console.log("Test request to test Cookie");
WindesheimRequest("Klas/1", data => {
  FetchAllClasses();

  subscribers.forEach(klas => {
    FetchClassLessen(klas, null, true);
  });

  setInterval(() => {
    subscribers.forEach(klas => {
      FetchClassLessen(klas, null, true);
    });
  }, 1000 * 60 * 10); // fetch every class every 10 minutes
});
